# Use the official NGINX image from the Docker Hub
FROM nginx:latest

# Copy the custom index.html to the default NGINX static content directory
COPY index.html /usr/share/nginx/html/index.html

# Expose port 80 to the outside world
EXPOSE 80

# Run NGINX in the foreground
CMD ["nginx", "-g", "daemon off;"]
